// Structs and Emums Demo
// Logan Coppock

#include <conio.h> //lets me use "_getch()"
#include <iostream> // stream to and from the console

using namespace std;

enum class Rank
{
	two = 2,
	three,
	four,
	five,
	six,
	seven,
	eight,
	nine,
	ten,
	Jack,
	Queen,
	King,
	Ace
};

enum Suit
{
	Spades,
	Hearts,
	Diamonds,
	Clubs
};

struct Card
{
	Rank rank;
	Suit suit;
};

int main()
{


	(void)_getch();
	return 0;
}